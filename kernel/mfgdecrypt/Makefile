include $(TOPDIR)/rules.mk

PKG_NAME:=mfgdecrypt
PKG_VERSION:=v3.1.1
SHORT_DESCRIPTION:=Kernel module to decrypt ciphered fields from the MFG

PKG_SOURCE:=mfg_decrypt-v3.1.1.tar.gz
PKG_SOURCE_URL:=https://gitlab.com/prpl-foundation/components/core/kmodules/mfg_decrypt/-/archive/v3.1.1
PKG_HASH:=5e1d90d2555a216ec90189929bfb38be8f57120db834456186dd3d998e7883fd
PKG_BUILD_DIR:=$(BUILD_DIR)/mfg_decrypt-v3.1.1
PKG_MAINTAINER:=Soft At Home <support.opensource@softathome.com>
PKG_LICENSE:=BSDGPL
PKG_LICENSE_FILES:=LICENSE


PKG_RELEASE:=1
PKG_BUILD_DEPENDS += kernel
PKG_BUILD_DEPENDS += dtc
PKG_EXTMOD_SUBDIRS+=src

include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/package.mk

define KernelPackage/$(PKG_NAME)
  CATEGORY:=prpl Foundation
  SUBMENU:=Kernel Modules
  TITLE:=$(SHORT_DESCRIPTION)
  FILES+=$(PKG_BUILD_DIR)/src/mfgdecrypt.$(LINUX_KMOD_SUFFIX)
  URL:=https://gitlab.com/prpl-foundation/components/core/kmodules/mfg_decrypt
  MENU:=1
endef

define KernelPackage/$(PKG_NAME)/description
	Kernel module to decrypt ciphered fields from the MFG
endef

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR) compile \
		COMPONENT=$(PKG_NAME) \
		COMPONENT_TOPDIR=$(PKG_BUILD_DIR) \
		STAGINGDIR=$(STAGING_DIR) \
		CONFIGDIR=$(STAGING_DIR) \
		LINUX_DIR=$(LINUX_DIR) \
		CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB=$(CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB) \
		CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG=$(CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG) \
		BUILD_DIR=$(PKG_BUILD_DIR) \
		$(MAKE_FLAGS) \
		$(KERNEL_MAKE_FLAGS)
endef

define Build/Install
	$(MAKE) -C $(PKG_BUILD_DIR) install \
		COMPONENT=$(PKG_NAME) \
		COMPONENT_TOPDIR=$(PKG_BUILD_DIR) \
		STAGINGDIR=$(STAGING_DIR) \
		CONFIGDIR=$(STAGING_DIR) \
		KMOD_INSTALL_PATH_PREFIX=$(PKG_INSTALL_DIR) \
		HARDCO_KERNEL_DIR=$(LINUX_DIR) \
		CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB=$(CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB) \
		CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG=$(CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG) \
		D=$(PKG_INSTALL_DIR) \
		DEST=$(PKG_INSTALL_DIR)
endef

define Build/InstallDev
	$(MAKE) -C $(PKG_BUILD_DIR) install \
		COMPONENT=$(PKG_NAME) \
		COMPONENT_TOPDIR=$(PKG_BUILD_DIR) \
		STAGINGDIR=$(STAGING_DIR) \
		CONFIGDIR=$(STAGING_DIR) \
		KMOD_INSTALL_PATH_PREFIX=$(STAGING_DIR) \
		HARDCO_KERNEL_DIR=$(LINUX_DIR) \
		CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB=$(CONFIG_SAH_MFG_DECRYPT_KEY_KERNEL_DTB) \
		CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG=$(CONFIG_SAH_MFG_DECRYPT_KEY_IN_MFG) \
		D=$(STAGING_DIR) \
		DEST=$(STAGING_DIR)
endef

define KernelPackage/$(PKG_NAME)/install
	$(CP) $(PKG_INSTALL_DIR)/* $(1)/
	if [ -d ./files ]; then \
		$(CP) ./files/* $(1)/; \
	fi
	find $(1) -name *.a -exec rm {} +;
	find $(1) -name *.h -exec rm {} +;
	find $(1) -name *.pc -exec rm {} +;
endef

define KernelPackage/$(PKG_NAME)/config
	source "$(SOURCE)/Config.in"
endef

$(eval $(call KernelPackage,$(PKG_NAME)))
